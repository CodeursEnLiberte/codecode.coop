---
layout: temoignage
nom: Yaal Coop
homepage: https://Yaal.coop/fr
date: 2024-10-30
---

## Est-ce que vous pouvez vous présenter ?

Nous sommes Yaal Coop, une SCIC (une Société Coopérative d'Intérêt Collectif). Nous avons créé l'entreprise en septembre 2020.
Nous sommes 5 depuis sa création, mais nous travaillions déjà ensemble chez Yaal SAS qui existait depuis une dizaine d'années.
Yaal SAS avait à l'époque une trentaine de salariés et a sorti un produit qui a très bien marché puis qui s'est fait racheter par une société suédoise. L'entreprise a aussi racheté une partie des contrats de travail. Sur les 10 personnes restantes, certaines ont changé de voie et 5 personnes ont décidé de rester et de créer Yaal Coop, une entreprise sur le modèle économique de Yaal SAS mais en tant que coopérative. Yaal SAS fonctionnait déjà selon certains principes coopératifs (avec notamment le principe de une personne = 1 voix) mais n'était pas formellement une coopérative.

Nous faisons de la prestation informatique auprès de nos clients, mais le but est surtout de réussir à trouver une idée de produit qui fonctionne bien et qui permette à tout le monde de travailler ensemble. [Canaille](https://gitlab.com/Yaal/canaille) est une tentative de ce type. On en avait besoin, donc on a travaillé dessus. Ce n'est pas forcément le produit qui nous permettra de générer assez de revenu pour que tout le monde puisse s'y consacrer à plein temps, mais c'est une possibilité.

Entre l'édition d'un projet 100% en interne et la prestation externe, il existe des solutions intermédiaires. Notre porte est toujours ouverte à des porteurs de projet externes qui ont besoin d'un partenaire technique pour la développer. Dans ces cas-là, il nous est possible de décider d'un investissement en argent dans le projet, dont le montant est à discuter et sur lequel on se met d'accord au début de l'association. L'argent mis dans la structure sert à payer de la presta auprès de Yaal Coop. Une fois l'argent épuisé, on espère que le projet dégage assez d'argent pour continuer à financer son développement. C'est un principe de fonctionnement qui vient de chez Yaal SAS.

## Qui sont les humains qui composent la coopérative ?

Nous avons des profils assez variés, nous sommes 3 hommes et 2 femmes. Nous recrutons actuellement.

Une personne qui nous rejoint est salariée pendant un an avant de devenir associé, il suffit pour cela d'acheter pour 100€ de parts. Le fait de devenir associé ne se substitue pas au salariat : la personne est à la fois salariée et associée.

À terme nous aimerions rester en nombre restreint, autour de 7 ou 8 personnes. Être 5 est un peu juste : le départ de quelqu'un a trop d'impact.

## Comment répartissez-vous les revenus ?

Tous les associés touchent un salaire unique et relativement bas. En fin d'année et en fonction des résultats, nous pouvons nous verser une prime adaptée aux résultats et à notre vision pour l'avenir. Tout le monde travaille à temps plein (35 heures par semaine).

## Comment animez-vous la vie de la coopérative ?

En tant que SCIC, Yaal Coop appartient à plusieurs collèges d'associés. Il y a le collège des salariés avec une répartition égale des parts entre les 5 salariés. Le collège des investisseurs est composé de certains anciens associés de Yaal SAS qui ont investi dans Yaal Coop pour aider l'entreprise à démarrer. Il y a également un collège des observateurs de 2 personnes. Il y a un collège des bénéficiaires qui compte aujourd'hui un membre : finacoop, notre cabinet comptable.

Nous fonctionnons selon les principes de l'holacratie avec des rôles qui tournent régulièrement. Par exemple, Président est un rôle qui tourne tous les 2 ans. Il y en a d'autres comme le rôle de "facteur" qui est la personne qui relève le courrier. Nous avons une réunion de gouvernance tous les 3 mois qui permet de faire tourner les rôles, d'en définir de nouveaux, ou d'en supprimer certains. Le but est de rendre visible toutes les tâches qui sont nécessaires pour faire tourner  la coopérative et de faciliter le passage d'un rôle à l'autre. Dans cette optique, les responsabilités liées à un rôle sont documentées.

Décider de la présidence se fait par une élection sans candidat, pour une durée de 2 ans. En pratique, tout le monde écrit un nom, puis explique pourquoi il a voté pour son candidat.

Il n'y a pas de directeur·ice général·e, car finalement le rôle n'a pas d'utilité pratique et cela représente un risque vis-à-vis du chômage. Le président peut avoir droit au chômage mais pas de manière automatique. Il faut le justifier auprès de France Travail et recommencer le dossier tous les 2 ans.

Nous n'organisons pas de séminaire car tous les membres de la coopérative travaillent au même endroit et se voient quotidiennement.

Nous avons entre nous des points récurrents :
- un point hebdo opérationnel qui porte sur le travail en cours
- une rétro trimestrielle qui permet de porter un regard sur le travail accompli les 3 derniers mois, de discuter de ce que l'on veut faire dans les 3 mois qui viennent. Et de partager son ressenti sur le trimestre qui vient de s'écouler.

Les prises de décision sont faites par consensus et discussion entre nous. Une décision n'est pas prise si quelqu'un est contre.

Les personnes qui arrivent ont accès à tous les outils internes, et sont considérés comme des associé·e·s même s'ils ou elles ne le sont pas encore. Nous faisons confiance aux personnes et considérons que la transparence aide à responsabiliser les gens. Par exemple avoir accès à la compta permet de prendre des décisions qui sont en adéquation avec l'état actuel des finances de l'entreprise.

## Quelles sont les principales difficultés que vous avez rencontrées ?

Trop de travail, pas assez de temps ! La plupart de membres sont passés par des phases temporaires de surcharge de travail. Le passage de salarié à associé dans une coopérative de 5 personnes a ajouté beaucoup de charge mentale et de stress lié à la gestion des finances de la boite.

Il y a un phénomène de montagne russe, avec des phases de calme, où tout va bien et des évènements imprévus qui arrivent en permanence, qui viennent bousculer l'équilibre de la coopérative. Typiquement le départ d'un membre de la coopérative ou un recrutement. Cela amène forcément un peu de stress.

## Quelles sont vos plus grandes fiertés ?

On est encore vivants ! Après la disparition de Yaal SAS, on aurait très bien pu disparaitre. On est heureux de travailler ensemble malgré les difficultés traversées. On arrive à sortir des produits cool, et nous sommes contents des projets sur lesquels nous travaillons. On avait exprimé l'envie d'être plus sélectifs que par le passé sur les projets dans lesquels on s'investit et pour l'instant on y arrive, on a pas eu à dévier de cet objectif. Nos projets actuels ont du sens pour nous.
