---
layout: temoignage
nom: DALIBO 
homepage: https://www.dalibo.com/
date: "2022-04-01"
---

[PostgreSQL]: https://www.postgresql.org
[Sponsor Majeur]: https://www.postgresql.org/about/sponsors/
[Temboard]: https://temboard.io/
[PostgreSQL Anonymizer]: https://labs.dalibo.com/postgresql_anonymizer
[pglift]: https://gitlab.com/dalibo/pglift
[Jean-Paul Argudo]: https://www.linkedin.com/in/jpargudo
[Damien Clochard]: https://www.linkedin.com/in/damienclochard

## Qui sommes-nous ?

DALIBO est une entreprise de services du numérique spécialiste de [PostgreSQL],
un logiciel libre de bases de données développé par une communauté 
internationnale. Nous sommes la seule société française à avoir le statut de
[Sponsor Majeur] du projet, aux cotés de Microsoft, Amazon, WMware et d'autres.

Nous proposons du support, de la formation et du conseil autour de ce logiciel
pour des clients dans tous les domaines d'activité : industrie, telecom, grande
distribution, ministères, etc.  Nous éditons également des logiciels libres 
pour faciliter l'usage de [PostgreSQL] en production, notamment [Temboard], 
[PostgreSQL Anonymizer] et [pglift].

Nous avons commencé à 4 en 2005 et aujourd'hui (2022) nous sommes une 
quarantaine de salarié·e·s, dont une vingtaine d'associé·e·s, à 100% en 
télétravail et réparti·e·s sur tout le territoire français.

Une de nos particularités, c'est d'avoir débuté avec le statut de SARL 
"classique"  puis de nous être reconvertis en SCOP en 2011.

## Pourquoi transformer une SARL en SCOP ?

La transformation d'une société en SCOP est souvent envisagée dans un contexte
de crise : soit parce que l'entreprise va mal financièrement et qu'elle est 
reprise en main par ses salariés pour la sauver, soit parce que le propriétaire 
souhaite partir et revendre la société.

Dans notre cas, il s'agit d'une transmission concertée, faite dans une période 
de bonne santé financière. A la fin des années 2000, la société grandit rapidement 
et les 2 co-fondateurs [Jean-Paul Argudo] et [Damien Clochard] font le constat 
que les statuts de la société ne correspondent pas à la réalité du terrain :

* Les salariés sont tous en télé-travail et disposent d'une forte autonomie
* Les bénéfices sont partagés en fin d'année
* Il y a déjà une forte transparence et une culture de la coopération, issues 
  des principes Open Source

Il faut donc faire évoluer les statuts l'entreprise pour reconnaître ces valeurs
et ces particularités. Après une année de recherche, la SARL est transformée
en société coopérative de production (SCOP). Concrètement les deux co-fondateurs
ouvrent le capital aux salarié·e·s et deviennent de facto minoritaires suivant le 
principe de "1 personne = 1 voix".


## Appliquer les principes Open Source à la gouvernance de la SCOP

Cette transformation se fait de manière concertée et dans une sorte d'évidence
car le modèle SCOP résonne très fort avec le modèle de développement des logiciels
libres, notamment :

* L'ouverture et l'inclusion de tous les acteurs
* Le partage des outils de production et des bénéfices
* La transparence comme fondement de la confiance
* La gouvernance collective

Au quotidien cela signifie que chaque associé·e de la SCOP, quels que soient son 
métier et son ancienneté, peut suivre les différents chantiers en cours et 
qu'il/elle peut s'impliquer dans un groupe de travail thématique ou dans un 
comité couvrant un domaine d'activité (formation, marketing, etc. ). Les assemblées
générales sont ouvertes aux salarié⋅e⋅s non-associé⋅e⋅s. Le partage des bénéfices 
est fait à parts égales, avec juste un pro-rata du temps de travail.

## Traiter l'entreprise comme un bien commun

Au final, c'est l'entreprise elle-même qui est considérée comme un outil commun,
comme une sorte de méga-logiciel-libre que les salarié·e·s peuvent faire évoluer 
de l'intérieur.

Au fil des années, les pratiques et l'organisation ont évolué pour s'adapter à 
la croissance et aux enjeux stratégiques. Ces changements se sont déroulés dans 
une dynamique collective, avec des débats parfois houleux et une grande diversité
de points de vue, mais toujours dans cet esprit de partage et de démocratie de la 
SCOP.

Cet esprit de coopération nous permet d'envisager l'avenir sereinement car il 
garantit à la fois la cohésion de l'ensemble et l'autonomie des individus. 




