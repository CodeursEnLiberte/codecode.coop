# Trame d’interview

1. Est-ce que vous pouvez vous présenter ?

- Quel est le statut de la structure ?
- Avez-vous des bureaux ?
- Êtes-vous tous dans la même ville ?
- Depuis combien de temps existez-vous ?
- Combien êtes-vous ?
- À qui appartient-elle ? (100% humain, ou bien aussi à des personnes morales ?)
- Faites vous parti d’une union, d’une fédération ou d’un groupement ?

2. Comment avez-vous eu l’idée de créer la structure ?

3. Qui sont les humains qui composent la coopérative ?

- Comment recrutez-vous ? (Notamment : y a-t-il une homogénéité de profil, de genre, de niveau d’expérience, d’école)
- Est-ce que vous recrutez actuellement ?
- Est-ce que tous vos salariés sont sociétaires ? (En particulier si la structure est une coopérative)
- Avez-vous tous le même nombre de parts ?

4. Comment répartissez-vous les revenus ?

- Comment cela se traduit-il pour le salarié (Présence de prime ? Qui la donne ? Participation ? Plan d’épargne retraite ?)
- Pourquoi avoir choisi ça ?
- Quel est votre temps de travail ? (Temps partiel ?)
- Accordez vous des pauses longues (Par exemple pour voyager, ou bien un congé parentalité, pour de l’activisme, pour lancer une activité, suivre une formation, ou simplement par oisiveté)
- Quelle solidarité avez-vous entre vous ?

5. Comment animez-vous la vie de la coopérative ?

- Comment est décidée la présidence ?
- Faites-vous des séminaires, des évènements hors des sites habituels ?
- Y a-t-il des rendez-vous récurrents (quotidiens, hebdomadaires, mensuels) ? En présentiel ou distanciel ?
- Quels outils utilisez-vous ?
- Comment prenez-vous les décisions (vote majoritaire, consensus…)
- Comment se passe l’accueil ? (notamment des juniors)
  - Est-ce que les personnes ont accès tous les outils (communication interne, compta, compte en banque?)
  - Est-ce qu’il y a un passage de l’historique formel ? (un wiki, un film d’entreprise)
  - Quelle est la répartition des tâches administratives ?

6. Quelles sont les principales difficultés que vous avez rencontrées ?

- Et les actuelles ?

7. Quelles sont vos plus grandes fiertés ?

8. Y a-t-il une particularité dans votre structure qu’on ne retrouve pas dans d’autres coopératives ?

9. Un mot de la fin ? Un projet pour le futur ?
